import play.*;
import play.mvc.*;
import controllers.*;
public class Global extends GlobalSettings {

  @Override
  public void onStart(Application app) {
    String message = "Violet Server online.";
    Logger.info(message);
    System.out.println(message);    
	}

  @Override
  public void onStop(Application app) {
        String message = "Violet Server shutting down....";
        Logger.info(message);
        System.out.println(message);
    }
}