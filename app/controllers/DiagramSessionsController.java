package controllers;

import java.util.*;
import play.mvc.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class DiagramSessionsController extends Controller {
    
    
    /**
     * VioletSession Inner Class
     * Used to represent a group diagram instance to keep track of serverside
     */
    private class VioletSession {
        List<String> commandArchive;
        HashSet<String> users;
        
        /**
         * Initialize the data structures we need
         */
        public VioletSession(){
            commandArchive = new ArrayList<>();
            users = new HashSet<>();
        }
        
        /**
         * Gets the users in the group. Unimplemented client-side, but left in code in case we ever need it.
         * @return Set<String> - a set that contains the users involved in the project
         */
        public Set<String> getUsers(){
            return users;
        }
        
        /**
         * Add a command to the list
         * @param String newCommand - the serialized command to store
         */
        public void addCommand(String newCommand){
            commandArchive.add(newCommand);
        }
        
        /**
         * Retrieve all commands since a given command was seen
         * @param int lastCommand - the command that was last seen
         * @return List<String> - A list of all the commands that were issued since the last seen command.
         */
        public List<String> getCommandsSince(int lastCommand){
            List<String> commandsSince = new ArrayList<>();
            int lastCmd = Integer.max(-1, lastCommand); //So it can be used in the loop
            for (int i = lastCmd + 1; i < commandArchive.size(); i++){
                commandsSince.add(commandArchive.get(i));
            }
            return commandsSince;
        }
    }
    
    // Stores the sessions. Uses TreeMap internally for alphanumeric sorting purposes.
    static Map<String, VioletSession> sessions = new TreeMap<>();
    
    /**
     * Constructor that initializes with a default session
     */
    public DiagramSessionsController(){
        sessions.put("default", new VioletSession()); //default violet session for testing
    }

    /*
     * Everything below here is a hook that is called whenever a query to the server is made through HTTP.
     * Queries are as configured in the conf/routes file. 
     */
    
    /**
     * get - retrieve commands
     * @param String groupname - the string name of the group, used as a key for retrieval
     * @param int lastCommandSeen - the last Command seen by the client making the request
     * @return Result( -DATA- ) - An HTTP "OK" response with body text containing all the serialized commands requested, separated by newlines and punctuated with an END.
     */
    public Result get(String groupname, int lastCommandSeen) {
        if (!sessions.containsKey(groupname)) {
            return ok("ERROR_GROUPNOTFOUND");
        }
        List<String> sessionCommandLog = sessions.get(groupname).getCommandsSince(lastCommandSeen);
        String requestedCommands = "";
        for (String serialObj : sessionCommandLog){
            requestedCommands = requestedCommands + serialObj + "\n";
        }
        requestedCommands = requestedCommands + "END";
        
        //Verbose
        System.out.println("Commands since #" + lastCommandSeen + " sent for " + groupname + ".");
        return ok(requestedCommands);
    }
    
    
    /**
     * put - send a command
     * @param String groupname - the name of the team diagram session being edited
     * @param String serialObj - the serialized command object to store in the session
     * @return Result - "OK" response with body that tells whether the operation succeeded or not
     */
    public Result put(String groupname, String serialObj){
        if (!sessions.containsKey(groupname)){
            return ok("ERROR_GROUPNOTFOUND"); //Group doesn't exist
        }
        if (serialObj.length() < 1){
            return ok("ERROR_OBJECTEMPTY"); //Command is empty
        }
        VioletSession targetSession = sessions.get(groupname);
        targetSession.addCommand(serialObj);
        
        //Verbose
        System.out.println("Command received for " + groupname + ".");
        return ok("SUCCESS");
    }

    /**
     * createSession - create a new Violet group session.
     * @param String groupname - the name of the group which will be used as a key for later retrieval.
     * @return Result - an "OK" HTTP response that reads "SUCCESS" in the body if there was no problems.
     */
    public Result createSession(String groupname){
        if (sessions.containsKey(groupname)){
            //Verbose
            System.out.println("Session '" + groupname + "': attempted to create, but already exists.");
            return ok("ERROR_GROUPEXISTS"); //Cannot have more than 1 of any given group name
        }
        sessions.put(groupname, new VioletSession());
        
        //Verbose
        System.out.println("Session '" + groupname + "' has been created.");
        
        return ok("SUCCESS");
    }
    
    /**
     * checkSession - See if a group exists.
     * @param String groupname - name of the group, used as a key.
     * @return Result - an HTTP "OK" response that contains body text detailing whether or not the group exists, as either "true" or "false".
     */
    public Result checkSession(String groupname){
        boolean exists = sessions.containsKey(groupname);
        
        String existStatus = "does not exist.";
        if (exists)
        {
            existStatus = "exists.";
        }

        //Verbose
        System.out.println("Session " + groupname + " checked: group " + existStatus);

        return ok(Boolean.toString(exists));
    }

    /**
     * getSessions - gets a list of all currently running group sessions.
     * @return Result - HTTP "OK" response with body text that shows which groups are available, in alphabetical order, separated by new lines.
     */
    public Result getSessions(){
        Set<String> groupNames = sessions.keySet();
        String resultBody = "";
        for (String name : groupNames){
            resultBody = resultBody + name + "\n";
        }
        resultBody = resultBody + "END";
        
        System.out.println("Group list requested. Sending...");
        return ok(resultBody);
    }
}
