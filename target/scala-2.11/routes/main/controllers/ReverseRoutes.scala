
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/bfg_chan/Documents/teamviolet_server/conf/routes
// @DATE:Sun May 14 13:54:22 PDT 2017

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:10
  class ReverseDiagramSessionsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:14
    def createSession(groupname:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "createsession/" + implicitly[PathBindable[String]].unbind("groupname", dynamicString(groupname)))
    }
  
    // @LINE:18
    def getSessions(): Call = {
    
      () match {
      
        // @LINE:18
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "grouplist")
      
      }
    
    }
  
    // @LINE:10
    def get(sessionid:String, lastcommandid:Int): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getcmds/" + implicitly[PathBindable[String]].unbind("sessionid", dynamicString(sessionid)) + "/" + implicitly[PathBindable[Int]].unbind("lastcommandid", lastcommandid))
    }
  
    // @LINE:16
    def checkSession(groupname:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "checkexists/" + implicitly[PathBindable[String]].unbind("groupname", dynamicString(groupname)))
    }
  
    // @LINE:12
    def put(sessionid:String, objectcode:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "sendcmd/" + implicitly[PathBindable[String]].unbind("sessionid", dynamicString(sessionid)) + "/" + implicitly[PathBindable[String]].unbind("objectcode", dynamicString(objectcode)))
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix)
    }
  
  }

  // @LINE:23
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:23
    def versioned(file:Asset): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:8
  class ReverseTestController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def get(key:Int): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "gettest/" + implicitly[PathBindable[Int]].unbind("key", key))
    }
  
  }


}
