
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/bfg_chan/Documents/teamviolet_server/conf/routes
// @DATE:Sun May 14 13:54:22 PDT 2017

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:10
  class ReverseDiagramSessionsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:14
    def createSession: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DiagramSessionsController.createSession",
      """
        function(groupname0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "createsession/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("groupname", encodeURIComponent(groupname0))})
        }
      """
    )
  
    // @LINE:18
    def getSessions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DiagramSessionsController.getSessions",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "grouplist"})
          }
        
        }
      """
    )
  
    // @LINE:10
    def get: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DiagramSessionsController.get",
      """
        function(sessionid0,lastcommandid1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getcmds/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("sessionid", encodeURIComponent(sessionid0)) + "/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("lastcommandid", lastcommandid1)})
        }
      """
    )
  
    // @LINE:16
    def checkSession: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DiagramSessionsController.checkSession",
      """
        function(groupname0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "checkexists/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("groupname", encodeURIComponent(groupname0))})
        }
      """
    )
  
    // @LINE:12
    def put: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DiagramSessionsController.put",
      """
        function(sessionid0,objectcode1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sendcmd/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("sessionid", encodeURIComponent(sessionid0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("objectcode", encodeURIComponent(objectcode1))})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:23
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:23
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:8
  class ReverseTestController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def get: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TestController.get",
      """
        function(key0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "gettest/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("key", key0)})
        }
      """
    )
  
  }


}
