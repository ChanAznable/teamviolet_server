
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/bfg_chan/Documents/teamviolet_server/conf/routes
// @DATE:Sun May 14 13:54:22 PDT 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:8
  TestController_3: controllers.TestController,
  // @LINE:10
  DiagramSessionsController_2: controllers.DiagramSessionsController,
  // @LINE:23
  Assets_1: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:8
    TestController_3: controllers.TestController,
    // @LINE:10
    DiagramSessionsController_2: controllers.DiagramSessionsController,
    // @LINE:23
    Assets_1: controllers.Assets
  ) = this(errorHandler, HomeController_0, TestController_3, DiagramSessionsController_2, Assets_1, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, TestController_3, DiagramSessionsController_2, Assets_1, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """gettest/""" + "$" + """key<[^/]+>""", """controllers.TestController.get(key:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getcmds/""" + "$" + """sessionid<[^/]+>/""" + "$" + """lastcommandid<[^/]+>""", """controllers.DiagramSessionsController.get(sessionid:String, lastcommandid:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sendcmd/""" + "$" + """sessionid<[^/]+>/""" + "$" + """objectcode<[^/]+>""", """controllers.DiagramSessionsController.put(sessionid:String, objectcode:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """createsession/""" + "$" + """groupname<[^/]+>""", """controllers.DiagramSessionsController.createSession(groupname:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """checkexists/""" + "$" + """groupname<[^/]+>""", """controllers.DiagramSessionsController.checkSession(groupname:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """grouplist""", """controllers.DiagramSessionsController.getSessions()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """grouplist/""", """controllers.DiagramSessionsController.getSessions()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """ An example controller showing a sample home page""",
      this.prefix + """"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_TestController_get1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("gettest/"), DynamicPart("key", """[^/]+""",true)))
  )
  private[this] lazy val controllers_TestController_get1_invoker = createInvoker(
    TestController_3.get(fakeValue[Int]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TestController",
      "get",
      Seq(classOf[Int]),
      "GET",
      """""",
      this.prefix + """gettest/""" + "$" + """key<[^/]+>"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_DiagramSessionsController_get2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getcmds/"), DynamicPart("sessionid", """[^/]+""",true), StaticPart("/"), DynamicPart("lastcommandid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DiagramSessionsController_get2_invoker = createInvoker(
    DiagramSessionsController_2.get(fakeValue[String], fakeValue[Int]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "get",
      Seq(classOf[String], classOf[Int]),
      "GET",
      """""",
      this.prefix + """getcmds/""" + "$" + """sessionid<[^/]+>/""" + "$" + """lastcommandid<[^/]+>"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_DiagramSessionsController_put3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sendcmd/"), DynamicPart("sessionid", """[^/]+""",true), StaticPart("/"), DynamicPart("objectcode", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DiagramSessionsController_put3_invoker = createInvoker(
    DiagramSessionsController_2.put(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "put",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """sendcmd/""" + "$" + """sessionid<[^/]+>/""" + "$" + """objectcode<[^/]+>"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_DiagramSessionsController_createSession4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("createsession/"), DynamicPart("groupname", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DiagramSessionsController_createSession4_invoker = createInvoker(
    DiagramSessionsController_2.createSession(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "createSession",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """createsession/""" + "$" + """groupname<[^/]+>"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_DiagramSessionsController_checkSession5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("checkexists/"), DynamicPart("groupname", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DiagramSessionsController_checkSession5_invoker = createInvoker(
    DiagramSessionsController_2.checkSession(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "checkSession",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """checkexists/""" + "$" + """groupname<[^/]+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_DiagramSessionsController_getSessions6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("grouplist")))
  )
  private[this] lazy val controllers_DiagramSessionsController_getSessions6_invoker = createInvoker(
    DiagramSessionsController_2.getSessions(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "getSessions",
      Nil,
      "GET",
      """""",
      this.prefix + """grouplist"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_DiagramSessionsController_getSessions7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("grouplist/")))
  )
  private[this] lazy val controllers_DiagramSessionsController_getSessions7_invoker = createInvoker(
    DiagramSessionsController_2.getSessions(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DiagramSessionsController",
      "getSessions",
      Nil,
      "GET",
      """""",
      this.prefix + """grouplist/"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_Assets_versioned8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned8_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:8
    case controllers_TestController_get1_route(params) =>
      call(params.fromPath[Int]("key", None)) { (key) =>
        controllers_TestController_get1_invoker.call(TestController_3.get(key))
      }
  
    // @LINE:10
    case controllers_DiagramSessionsController_get2_route(params) =>
      call(params.fromPath[String]("sessionid", None), params.fromPath[Int]("lastcommandid", None)) { (sessionid, lastcommandid) =>
        controllers_DiagramSessionsController_get2_invoker.call(DiagramSessionsController_2.get(sessionid, lastcommandid))
      }
  
    // @LINE:12
    case controllers_DiagramSessionsController_put3_route(params) =>
      call(params.fromPath[String]("sessionid", None), params.fromPath[String]("objectcode", None)) { (sessionid, objectcode) =>
        controllers_DiagramSessionsController_put3_invoker.call(DiagramSessionsController_2.put(sessionid, objectcode))
      }
  
    // @LINE:14
    case controllers_DiagramSessionsController_createSession4_route(params) =>
      call(params.fromPath[String]("groupname", None)) { (groupname) =>
        controllers_DiagramSessionsController_createSession4_invoker.call(DiagramSessionsController_2.createSession(groupname))
      }
  
    // @LINE:16
    case controllers_DiagramSessionsController_checkSession5_route(params) =>
      call(params.fromPath[String]("groupname", None)) { (groupname) =>
        controllers_DiagramSessionsController_checkSession5_invoker.call(DiagramSessionsController_2.checkSession(groupname))
      }
  
    // @LINE:18
    case controllers_DiagramSessionsController_getSessions6_route(params) =>
      call { 
        controllers_DiagramSessionsController_getSessions6_invoker.call(DiagramSessionsController_2.getSessions())
      }
  
    // @LINE:20
    case controllers_DiagramSessionsController_getSessions7_route(params) =>
      call { 
        controllers_DiagramSessionsController_getSessions7_invoker.call(DiagramSessionsController_2.getSessions())
      }
  
    // @LINE:23
    case controllers_Assets_versioned8_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned8_invoker.call(Assets_1.versioned(path, file))
      }
  }
}
