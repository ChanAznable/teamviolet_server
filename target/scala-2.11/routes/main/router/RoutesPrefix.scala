
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/bfg_chan/Documents/teamviolet_server/conf/routes
// @DATE:Sun May 14 13:54:22 PDT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
